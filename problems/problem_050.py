# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
import math

import math

def halve_the_list(list):
    length = len(list)
    if length % 2 == 0:
        mid = len(list)/2
        arr1 = list[:int(mid)]
        arr2 = list[int(mid):]
    else:
        mid = math.ceil(len(list)/2)
        arr1 = list[:int(mid)]
        arr2 = list[int(mid):]
        
    return arr1, arr2

halve_the_list([1, 2, 3, 4, 5])
