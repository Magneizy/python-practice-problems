# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

from tkinter.messagebox import RETRY


def calculate_sum(values):
    total = 0
    if len(values) == 0:
        return None
    else:
        for i in values:
            total += int(i)
        
    return total
    pass
