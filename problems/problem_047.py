# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    spec_char = ['$', '!', '@']
    val1 = False
    val2 = False
    val3 = False
    val4 = False
    val5 = False
    val6 = False
    num = 0
    length = len(password)
    if (5 < length < 13):
        val1 = True
    for i in password:
        if i.isdigit():
            val2=True
        if i.isalpha():
            val3 = True
        if i.isupper():
            val4 = True
        if i.islower():
            val5 = True
        if i in spec_char:
            num += 1
            if num > 0:
                val6 = True
    return (val1 and val2 and val3 and val4 and val5 and val6)

# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

# def check_password(password):
#     has_lowercase_letter = False                                        # solution
#     has_uppercase_letter = False                                        # solution
#     has_digit = False                                                   # solution
#     has_special_char = False                                            # solution
#     for character in password:                                          # solution
#         if character.isalpha():                                         # solution
#             if character.isupper():                                     # solution
#                 has_uppercase_letter = True                             # solution
#             else:                                                       # solution
#                 has_lowercase_letter = True                             # solution
#         elif character.isdigit():                                       # solution
#             has_digit = True                                            # solution
#         elif character == "$" or character == "!" or character == "@":  # solution
#             has_special_char = True                                     # solution
#     return (                                                            # solution
#         len(password) >= 6                                              # solution
#         and len(password) <= 12                                         # solution
#         and has_lowercase_letter                                        # solution
#         and has_uppercase_letter                                        # solution
#         and has_digit                                                   # solution
#         and has_special_char                                            # solution
#     )                                                                   # solution
#     # pass                                                              # problem
